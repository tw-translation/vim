#!/usr/bin/env python3
import os
source = "zh_TW.po"
source_encoding = "UTF-8"
target = "zh_TW.Big5.po"
target_encoding = "Big5"
print("Started.")
os.system("iconv -f {se} -t {te} -o {target} {source}".format(source=source, se = source_encoding, target=target, te = target_encoding))
print("Done.")
exit()
